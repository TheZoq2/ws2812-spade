module top
    ( input clk
    , output[7:0] pmod0
    );

    reg rst = 1;
    always @(posedge clk) begin
        rst <= 0;
    end

    \ws2812::hw_test::demo main
        ( .clk_i(clk)
        , .rst_i(rst)
        , .output__(pmod0[0])
        );
endmodule
